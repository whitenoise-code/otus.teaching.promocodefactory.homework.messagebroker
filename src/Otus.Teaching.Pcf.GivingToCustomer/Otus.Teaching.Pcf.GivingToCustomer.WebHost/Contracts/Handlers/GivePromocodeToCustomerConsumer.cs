﻿using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;

namespace Pcf.Integration.Contracts.Handlers
{
    public class GivePromocodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomer>
    {
        private readonly IPromocodesService _promocodesService;
        private readonly ILogger<GivePromocodeToCustomerConsumer> _logger;

        public GivePromocodeToCustomerConsumer(
            IPromocodesService promocodesService,
            ILogger<GivePromocodeToCustomerConsumer> logger)
        {
            _promocodesService = promocodesService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomer> context)
        {
            _logger.LogInformation("Обработка события {message}", context.Message);

            var request = new GivePromoCodeRequest
            {
                BeginDate = context.Message.BeginDate,
                EndDate = context.Message.EndDate,
                PartnerId = context.Message.PartnerId,
                PreferenceId = context.Message.PreferenceId,
                PromoCode = context.Message.PromoCode,
                ServiceInfo = context.Message.ServiceInfo,
                PromoCodeId = context.Message.PromoCodeId
            };
            await _promocodesService.GivePromoCodesToCustomersAsync(request);
        }
    }
}
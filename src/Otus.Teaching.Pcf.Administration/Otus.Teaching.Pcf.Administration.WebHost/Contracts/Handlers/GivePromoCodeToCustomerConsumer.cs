﻿using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.WebHost;

namespace Pcf.Integration.Contracts.Handlers
{
    public class GivePromoCodeToCustomerConsumer : IConsumer<GivePromoCodeToCustomer>
    {
        private readonly IEmployeeService _employeeService;
        private readonly ILogger<GivePromoCodeToCustomerConsumer> _logger;

        public GivePromoCodeToCustomerConsumer(
            IEmployeeService employeeService,
            ILogger<GivePromoCodeToCustomerConsumer> logger)
        {
            _employeeService = employeeService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomer> context)
        {
            _logger.LogInformation("Обработка события {message}", context.Message);

            if (!context.Message.PartnerManagerId.HasValue)
                return;

            await _employeeService.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId.Value);
        }
    }
}
﻿using System;

namespace Pcf.Integration.Contracts
{
    public interface GivePromoCodeToCustomer
    {
        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }
        
        public Guid PromoCodeId { get; set; }

        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }
        
        public Guid? PartnerManagerId { get; set; }
    }
}
﻿using System;

namespace Otus.Teaching.Pcf.WebRazor.Models.Giving
{
    public class PromoCodeShort
    {
        public Guid Id { get; set; }

        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }

        public Guid PartnerId { get; set; }
    }
}
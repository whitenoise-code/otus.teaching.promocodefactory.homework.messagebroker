﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.WebRazor.Models.Receiving
{
    public class Partner
    {
        public Guid Id { get; set; }

        public bool IsActive { get; set; }

        public string Name { get; set; }

        public int NumberIssuedPromoCodes  { get; set; }

        public List<PromocodeLimit> PartnerLimits { get; set; }
    }
}
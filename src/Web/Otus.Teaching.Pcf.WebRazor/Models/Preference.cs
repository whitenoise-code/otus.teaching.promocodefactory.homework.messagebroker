﻿using System;

namespace Otus.Teaching.Pcf.WebRazor.Models.Giving
{
    public class Preference
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
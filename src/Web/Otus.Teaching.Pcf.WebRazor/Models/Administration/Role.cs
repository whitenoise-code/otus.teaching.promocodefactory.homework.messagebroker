﻿using System;

namespace Otus.Teaching.Pcf.WebRazor.Models.Administration
{
    public class Role
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
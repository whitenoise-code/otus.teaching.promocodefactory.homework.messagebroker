﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.WebRazor.Models.Giving;
using Otus.Teaching.Pcf.WebRazor.Models.Receiving;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Services
{
    public class ReceivingFromPartnerService : IReceivingFromPartnerService
    {
        private readonly HttpClient _client;

        public ReceivingFromPartnerService(HttpClient client)
        {
            _client = client;
        }

        public async Task<List<Partner>> GetPartnersAsync()
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/partners");
            if (!response.IsSuccessStatusCode) return null;
            var customers = JsonConvert.DeserializeObject<List<Partner>>(await response.Content.ReadAsStringAsync());
            return customers;
        }

        public async Task<Partner> GetPartnerAsync(Guid id)
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/partners/{id}");
            if (!response.IsSuccessStatusCode) return null;
            var customer = JsonConvert.DeserializeObject<Partner>(await response.Content.ReadAsStringAsync());
            return customer;
        }

        public async Task SetPartnerPromoCodeLimitAsync(Guid id, SetLimitRequest request)
        {
            await _client.PostAsync($"{_client.BaseAddress}api/v1/partners/{id}/limits",
                new StringContent(JsonConvert.SerializeObject(request)));
        }

        public async Task<PromocodeLimit> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/partners/{id}/limits/{limitId}");
            if (!response.IsSuccessStatusCode) return null;
            var result = JsonConvert.DeserializeObject<PromocodeLimit>(await response.Content.ReadAsStringAsync());
            return result;
        }

        public async Task CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            await _client.PostAsJsonAsync($"{_client.BaseAddress}api/v1/partners/{id}/canceledLimits", (object)null);
        }

        public async Task<List<PromoCodeShort>> GetPartnerPromoCodesAsync(Guid id)
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/partners/{id}/promocodes");
            if (!response.IsSuccessStatusCode) return null;
            var customers = JsonConvert.DeserializeObject<List<PromoCodeShort>>(await response.Content.ReadAsStringAsync());
            return customers;
        }

        public async Task<PromoCodeShort> GetPartnerPromoCodeAsync(Guid id, Guid promoCodeId)
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/partners/{id}/promocodes{promoCodeId}");
            if (!response.IsSuccessStatusCode) return null;
            var customer = JsonConvert.DeserializeObject<PromoCodeShort>(await response.Content.ReadAsStringAsync());
            return customer;
        }

        public async Task ReceivePromoCodeFromPartnerWithPreferenceAsync(Guid id, ReceivePromocodeRequest request)
        {
            var res = await _client.PostAsJsonAsync($"{_client.BaseAddress}api/v1/partners/{id}/promocodes",request);
            var content = await res.Content.ReadAsStringAsync();
        }

        public async Task<List<Preference>> GetPreferencesAsync()
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/preferences");
            if (!response.IsSuccessStatusCode) return null;
            var customers = JsonConvert.DeserializeObject<List<Preference>>(await response.Content.ReadAsStringAsync());
            return customers;
        }
    }
}
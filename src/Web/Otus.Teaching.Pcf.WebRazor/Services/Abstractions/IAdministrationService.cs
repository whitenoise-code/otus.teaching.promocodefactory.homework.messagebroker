﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.WebRazor.Models.Administration;

namespace Otus.Teaching.Pcf.WebRazor.Services.Abstractions
{
    public interface IAdministrationService
    {
        Task<List<EmployeeShort>> GetEmployeesAsync();
        Task<Employee> GetEmployeeByIdAsync(Guid id);
        Task UpdateAppliedPromocodesAsync(Guid employeeId);

        Task<List<Role>> GetRolesAsync();
    }
}
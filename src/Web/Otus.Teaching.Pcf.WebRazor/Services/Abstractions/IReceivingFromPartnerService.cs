﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.WebRazor.Models.Giving;
using Otus.Teaching.Pcf.WebRazor.Models.Receiving;

namespace Otus.Teaching.Pcf.WebRazor.Services.Abstractions
{
    public interface IReceivingFromPartnerService
    {
        Task<List<Partner>> GetPartnersAsync();
        Task<Partner> GetPartnerAsync(Guid id);
        Task SetPartnerPromoCodeLimitAsync(Guid id, SetLimitRequest request);
        Task<PromocodeLimit> GetPartnerLimitAsync(Guid id, Guid limitId);
        Task CancelPartnerPromoCodeLimitAsync(Guid id);
        Task<List<PromoCodeShort>> GetPartnerPromoCodesAsync(Guid id);
        Task<PromoCodeShort> GetPartnerPromoCodeAsync(Guid id, Guid promoCodeId);
        Task ReceivePromoCodeFromPartnerWithPreferenceAsync(Guid id, ReceivePromocodeRequest request);

        Task<List<Preference>> GetPreferencesAsync();
    }
}
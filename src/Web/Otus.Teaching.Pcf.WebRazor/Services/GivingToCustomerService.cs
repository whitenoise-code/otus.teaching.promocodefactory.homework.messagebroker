﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.WebRazor.Models.Giving;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Services
{
    public class GivingToCustomerService : IGivingToCustomerService
    {
        private readonly HttpClient _client;

        public GivingToCustomerService(HttpClient client)
        {
            _client = client;
        }

        public async Task<List<CustomerShort>> GetAllAsync()
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/customers");
            if (!response.IsSuccessStatusCode) return null;
            var customers =
                JsonConvert.DeserializeObject<List<CustomerShort>>(await response.Content.ReadAsStringAsync());
            return customers;
        }

        public async Task<Customer> GetCustomerAsync(Guid id)
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/customers/{id}");
            if (!response.IsSuccessStatusCode) return null;
            var customer = JsonConvert.DeserializeObject<Customer>(await response.Content.ReadAsStringAsync());
            return customer;
        }

        public async Task CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            await _client.PostAsJsonAsync($"{_client.BaseAddress}api/v1/customers", request);
        }

        public async Task UpdateCustomerAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var res = await _client.PutAsJsonAsync($"{_client.BaseAddress}api/v1/customers/{id}", request);
        }

        public async Task DeleteCustomerAsync(Guid id)
        {
            var result = await _client.DeleteAsync($"{_client.BaseAddress}api/v1/customers/{id}");
        }

        public async Task<List<PromoCodeShort>> GetAllPromocodesAsync()
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/promocodes");
            if (!response.IsSuccessStatusCode) return null;

            var customers =
                JsonConvert.DeserializeObject<List<PromoCodeShort>>(await response.Content.ReadAsStringAsync());
            return customers;
        }

        public async Task GivePromoCodesToCustomersAsync(GivePromoCodeRequest request)
        {
            await _client.PostAsync($"{_client.BaseAddress}api/v1/promocodes",
                new StringContent(JsonConvert.SerializeObject(request)));
        }

        public async Task<List<Preference>> GetAllPreferences()
        {
            var response = await _client.GetAsync($"{_client.BaseAddress}api/v1/preferences");
            if (!response.IsSuccessStatusCode) return null;

            var customers = JsonConvert.DeserializeObject<List<Preference>>(await response.Content.ReadAsStringAsync());
            return customers;
        }
    }
}
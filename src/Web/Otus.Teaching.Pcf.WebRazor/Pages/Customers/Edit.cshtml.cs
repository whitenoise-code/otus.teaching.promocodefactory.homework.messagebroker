﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.Pcf.WebRazor.Models.Giving;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Pages.Customers
{
    public class EditModel : PageModel
    {
        private readonly IGivingToCustomerService _givingService;

        public EditModel(IGivingToCustomerService givingService)
        {
            _givingService = givingService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public List<SelectListItem> ListItems { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            var customer = await _givingService.GetCustomerAsync(id);
            Input = customer.Adapt<InputModel>();

            var preferences = await _givingService.GetAllPreferences();
            var ids = customer.Preferences.Select(p => p.Id);
            ListItems = preferences
                .Select(preference => new SelectListItem
                {
                    Value = preference.Id.ToString(),
                    Text = preference.Name,
                    Selected = ids.Contains(preference.Id)
                }).ToList();

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var request = Input.Adapt<CreateOrEditCustomerRequest>();
            request.PreferenceIds = Input.SelectedValues.Select(Guid.Parse).ToList();
            await _givingService.UpdateCustomerAsync(id, request);

            return RedirectToPage(nameof(Index));
        }

        public class InputModel
        {
            [Required]
            public Guid Id { get; set; }

            [Required]
            [Display(Name = "Имя")]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "Имя")]
            public string LastName { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "Адрес электронной почты")]
            public string Email { get; set; }

            public IEnumerable<string> SelectedValues { get; set; }
        }
    }
}
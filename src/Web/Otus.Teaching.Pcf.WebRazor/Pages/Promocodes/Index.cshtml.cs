﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.WebRazor.Models.Giving;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Pages.Promocodes
{
    public class Index : PageModel
    {
        private readonly IGivingToCustomerService _givingToCustomerService;

        public Index(IGivingToCustomerService givingToCustomerService)
        {
            _givingToCustomerService = givingToCustomerService;
        }

        public IEnumerable<PromoCodeShort> Promocodes { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            Promocodes = await _givingToCustomerService.GetAllPromocodesAsync();

            return Page();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.Pcf.WebRazor.Models.Receiving;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Pages.Promocodes
{
    public class Create : PageModel
    {
        private readonly IReceivingFromPartnerService _receivingFromPartnerService;

        public Create(IReceivingFromPartnerService receivingFromPartnerService)
        {
            _receivingFromPartnerService = receivingFromPartnerService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public List<SelectListItem> ListItems { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            var preferences = await _receivingFromPartnerService.GetPreferencesAsync();

            ListItems = preferences
                .Select(preference => new SelectListItem
                {
                    Value = preference.Id.ToString(),
                    Text = preference.Name
                }).ToList();

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid partnerId)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var request = Input.Adapt<ReceivePromocodeRequest>();
            await _receivingFromPartnerService.ReceivePromoCodeFromPartnerWithPreferenceAsync(partnerId, request);

            return RedirectToPage(nameof(Index));
        }

        public class InputModel
        {
            [Required]
            public string ServiceInfo { get; set; }

            [Required]
            public string PromoCode { get; set; }

            [Required]
            public Guid PreferenceId { get; set; }

            public Guid? PartnerManagerId { get; set; }

        }
    }
}
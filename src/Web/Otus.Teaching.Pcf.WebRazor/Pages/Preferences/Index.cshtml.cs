﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Otus.Teaching.Pcf.WebRazor.Models.Giving;
using Otus.Teaching.Pcf.WebRazor.Services.Abstractions;

namespace Otus.Teaching.Pcf.WebRazor.Pages.Preferences
{
    public class Index : PageModel
    {
        private readonly IGivingToCustomerService _givingToCustomerService;

        public Index(IGivingToCustomerService givingToCustomerService)
        {
            _givingToCustomerService = givingToCustomerService;
        }

        public List<Preference> Preferences { get; set; }


        public async Task OnGetAsync()
        {
            Preferences = await _givingToCustomerService.GetAllPreferences();
        }
    }
}
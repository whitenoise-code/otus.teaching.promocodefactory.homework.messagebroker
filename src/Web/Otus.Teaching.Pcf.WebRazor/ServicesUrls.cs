﻿namespace Otus.Teaching.Pcf.WebRazor
{
    public class ServicesUrls
    {
        public string Administration { get; set; }
        public string GivingToCustomer { get; set; }
        public string ReceivingFromPartner { get; set; }
    }
}